#解析

require 'rexml/document'

doc = REXML::Document.new(File.new(__dir__ + '/fixture/shop.xml'))

# initialize the total to 0 as a float
total = 0.0

# cycle through the items
doc.elements.each('shopping/item') do |item|

  # add the price to the total
  total += item.attributes['price'].to_f * item.attributes['quantity'].to_i
end

# round the total to the nearest 0.01
total = (total*100.0).round/100.0

# pad the output with the proper number of trailing 0's
printf "$%.2f\n", total
