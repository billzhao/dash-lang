#数学－两点直接的距离
distance = Math.hypot(1-1, 2-2)
puts distance

#输出－数字补全
puts 42.to_s.rjust(8, "0")
puts 1024.to_s.ljust(6)
puts 73.to_s.rjust(10)

#输出－格式化十进制小数
puts (7.0/8.0*100).round/100.0

#随机数
puts rand(200-100+1) + 100
