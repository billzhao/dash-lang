# Range Sum Query - Immutable

# https://leetcode.com/problems/range-sum-query-immutable/

# Code

class NumArray

  # Initialize your data structure here.
  # @param {Integer[]} nums
  def initialize(nums)

    tmp = {}

    nums.each_with_index { |item, index| tmp[index] = (tmp[index - 1] || 0)  + item }

    @nums = tmp

  end

  # @param {Integer} i
  # @param {Integer} j
  # @return {Integer}
  def sum_range(i, j)
    case
      when i > j
        return 0
      when i == 0
        return @nums[j]
      else
        return @nums[j] - @nums[i-1]
    end
  end
end

num_array = NumArray.new([-2, 0, 3, -5, 2, -1])
p num_array.sum_range(0, 2)
p num_array.sum_range(2, 5)
p num_array.sum_range(0, 5)
