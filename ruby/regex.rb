#模式匹配－验证字符串是否匹配一个正则表达式
puts "ok" if ("Hello"=~/^[A-Z][a-z]+$/)

#模式匹配－模式获取
puts "#{$1} #{$2}" if "one two three jump"=~/^one (.*) three (.*)$/

#查询－判断字符串是否包含一个正则表达式
puts "ok" if ("/abc 123 @#$/"=~/\d+/)

#查询－基于匹配进行字符串格式化操作
"(fish):1 sausage (cow):3 tree (boat):4".scan(/\((\w+)\):(\d+)/).each{|entity,count| puts entity.to_s.ljust(8) + count.to_s.rjust(4, "0")}

#替换－替换第一次满足条件的静态字符串
p "Red Green Blue".sub(/e/,'*')

#替换－替换所有满足条件的静态字符串
p "She sells sea shells".gsub(/se\w+/,"X")

#替换－字符串动态替换
p "The {Quick} Brown {Fox}".gsub(/\{(\w+)\}/).each { |key|  key[1..-2].reverse}
