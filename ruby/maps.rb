#定义
p {}
p {}.freeze

#定义－初始化
shapes = {'circle'=>1, 'triangle'=>3, 'square'=>4}
p shapes
shapes = Hash['circle', 1, 'triangle', 3, 'square', 4]
p shapes


#访问－判断key是否存在
puts "ok" if shapes.has_key?('circle')

#访问－获取某个key的元素
puts shapes['circle'] if shapes.has_key?('circle')

#访问－修改键值
shapes['circle'] = 10 and puts shapes['circle']

#访问－修改某个键值
maps = {'joe'=>"cat",'mary' => "turtle",'bill' => "canary"}
puts maps.delete "bill"

#算法－统计字符串的Count
histogram = {}
['a','b','a','c','b','b'].each { |item| histogram[item] = (histogram[item] || 0) +1 }
puts histogram

#算法－归类－字符size
lengths = ["one", "two", "three", "four", "five"].group_by {|x| x.size}
puts lengths
#算法－归类－元素类型
instances = ["one", "two", 1, [1,2], {'hello' => 1}].group_by {|x| x.class}
puts instances
