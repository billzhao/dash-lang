#基础－定义类
class Greeter

  attr_accessor :whom

  def initialize(whom)
    @whom = whom
  end

  def greet()
    puts "Hello, #{@whom}"
  end

end

(Greeter.new("泥煤")).greet()

#基础－实例多态
greeter = Greeter.new("泥煤" )
greeter.greet()
greeter.whom = "World"
greeter.greet()


#继承
class Shape
  def initialize(name = "")
    @name = name
  end
end

class Circle < Shape
  def initialize(radius) super("circle")
    @radius = radius
  end

  def area() Math::PI * @radius * @radius end
  def circumference() 2 * Math::PI * @radius end

  def print()
    puts "I am a #{@name} with ->"
    puts "Radius:        %.2f" % @radius
    puts "Area:          %.2f" % self.area()
    puts "Circumference: %.2f\n" % self.circumference()
  end

end

class Rectangle < Shape
  def initialize(length, breadth) super("rectangle") ; @length = length ; @breadth = breadth  end

  def area() @length * @breadth end
  def perimeter() 2 * @length + 2 * @breadth end

  def print()
    puts "I am a #{@name} with ->"
    printf("Length, Width: %.2f, %.2f\n", @length, @breadth)
    puts "Area:          %.2f" % self.area()
    puts "Perimeter:     %.2f\n" % self.perimeter()
  end
end

puts "PI :          %.8f" % Math::PI

shapes = [Circle.new(4.2), Rectangle.new(2.7, 3.1), Rectangle.new(6.2, 2.6), Circle.new(17.3)]
shapes.each {|shape| shape.print}

#接口－对象序列化
class Person
  def initialize(name, age)
    @name, @age = name, age
  end
end

tom = Person.new("Tom Bones", 23)

File.open('test_file', 'w+') {|f| f.write(Marshal.dump(tom)) }
toms_clone = Marshal.load(File.read('test_file'))
