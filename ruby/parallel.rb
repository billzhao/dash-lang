#工作池

array, threads, answers = ["ab", "we", "tfe", "aoj"], [], []
array.each { |word|
  threads << Thread.new(word.split '' ) do |x|
    answer = []
    x.each { |a|
      answer << a
      x.each { |b| answer << [a, b].join }
    }
    answers << answer
  end
}
threads.each {|thr| thr.join}

puts answers

#多线程
%w[one two three four].each do |number|
  Thread.new(number) { |number|
    puts "Thread #{number} says Hello World!"
  }.join
end
