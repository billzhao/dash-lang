#文件－读取
file = File.new(__FILE__)
puts file.read

#文件－扫描
File.open(__FILE__).each_with_index { |line, index|
  puts "#{index+1} > #{line}"
}

#文件－写
File.new("test_file", 'w') << "some text"
IO.write("test_file", "some text that overwrites")

file = File.new("test_file", 'a+')
file.puts "\nfffff"
file.close

#文件－目录扫描
Dir.foreach(__dir__) {
  |file|
    unless ['.', '..'].include?(file)
      case
        when File.directory?(file)
          then Dir.foreach(file) {|item|  puts "---#{file}\n-------#{item}" unless ['.', '..'].include?(item)}
        else
          puts '---' + file
      end
    end
}

#文件－递归扫描
def procdir(dirname)
  Dir.foreach(dirname) do |dir|
    dirpath = dirname + '/' + dir
    if File.directory?(dirpath) then
      if dir != '.' && dir != '..' then
        puts "DIRECTORY: #{dirpath}" ; procdir(dirpath)
      end
    else
      puts "FILE: #{dirpath}"
    end
  end
end

procdir(__dir__)
