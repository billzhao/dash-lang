#定义
puts [1,2,3]
puts ['wo', 'ai', 'ni']

#输出－切割字符串
puts ['wo', 'ai', 'ni'].join(', ')

#输出－
def join(arr)
  return '' if not arr
  case arr.size
    when 0 then ''
    when 1 then arr[0]
    when 2 then arr.join(' and ')
    else arr[0..-2].join(', ') + ', and ' + arr[-1]
  end
end

puts join(["wo", "ai", "ni"])
puts join(["wo", "ai"])
puts join(["wo"])

#访问
list = ['One', 'Two', 'Three', 'Four', 'Five']
puts list.at(2)
puts list[2]
puts list.fetch(2)

beans   = ['red', 'white', 'black']
colors  = ['red']

p (beans & colors)

ages = [18, 16, 17, 18, 16, 19, 14, 17, 19, 18]
ages.uniq!
p ages

#修改
list = ['Apple', 'Banana', 'Carrot']
list.delete_at(-1)

p list

list = ['Apple', 'Banana', 'Carrot']
list.shift

p list

items = ["apple", "orange", "grapes", "bananas"]
items << first = items.shift

p first

#操作－组合
first = ['Bruce', 'Tommy Lee', 'Bruce']; last = ['Willis', 'Jones', 'Lee']; years = [1955, 1946, 1940]
p first.zip(last, years)
p [first, last, years].transpose

#操作－Map
p ["ox", "cat", "deer", "whale"].map{|i| i.length}

#操作－过滤数据
things=["hello", 25, 3.14, Time.now]
p things.select{|i| i.is_a? Numeric}

#测试
p [2, 3, 4].all? { |x| x > 1 }
p [2, 3, 4].any? { |x| x > 3 }
