#终端打印
puts "Hello World!"

#字符处理
strings = "The quick brown fox jumps over the lazy dog. " * 10

strings = strings.scan(/[^ ].{0,76}/).each{ |line| puts "> %s" % line }

#Demo
strings = "The quick brown fox jumps over the lazy dog. "
strings.split.each do |word|
  puts "%s" % word
end

#特殊字符
puts '\#{\'}${"}/'

#块文本
puts <<"HERE"
This
Is
A
Multiple
String
HERE

#字符串＋表达式
a = 1
b = 3
puts "#{a} + #{b} = #{a+b}"

#字符反转
puts "我爱大中国".reverse

#字符反转-单词
puts "This is a end, my only friend !".split.reverse.join(' ')

#trim
puts "Hello    ".strip

#大小写
puts "Space Monkey".upcase
puts "Space Monkey".downcase

#首字母大写
text = "man of stEEL"
puts text.split.each { |i| i.capitalize! }.join(' ')
