#条件－IF..THEN
name = 'Bill'

if (name == 'Bill')
  puts '~Elegant~'
end

puts "~Elegant~" if name == 'Bill'

#条件－IF..THEN..ELSE
age = 29

if age > 30
  puts "老男人"
else
  puts "小鲜肉"
end

puts age > 30 ? "老男人" : "小鲜肉"

puts "你是 #{age > 30 ? "老男人" : "小鲜肉"}"

#条件－IF..THEN..ELSIF..ELSE
if age > 30
  puts "老男人"
elsif age > 20
  puts "小马达"
else
  puts "小鲜肉"
end

#条件－Case
case
  when age > 30 then puts "老男人"
  when age > 20 then puts "小马达"
  else
    puts "小鲜肉"
end

#suffix number
def suffixed(number)
  last_digit = number.to_s[-1..-1].to_i
  suffix = case last_digit
             when 1 then 'st'
             when 2 then 'nd'
             when 3 then 'rd'
             else 'th'
           end
  suffix = 'th' if (11..13).include?(number)
  "#{number}#{suffix}"
end

(1..30).each { |n| puts suffixed(n)}

#循环
x=1
  while x < 150
    puts x
    x <<= 1 #double x
  end

x=1
while x < 10
  puts x
  x += 1
end

#循环－随机数
rnd = 0
while (rnd != 6)
  rnd = rand(6)+1
  print rnd
  print "," if (rnd!=6)
end

puts

begin
  rnd = rand(6)+1
  print rnd
  print "," if rnd!=6
end while rnd != 6

#循环
puts "Hello" * 5
5.times { print "Hello" }

puts

a = 10
a.downto(1) { |n| print n, " .. " }
puts "Liftoff!"
